import sys
import os
import re
import argparse
import json
from pathlib import Path
from datetime import datetime
from shutil import copy

from Crypto.Cipher import AES
import hashlib

from urllib.request import urlopen, urlretrieve
from urllib.error import URLError
# from bs4 import BeautifulSoup
import xml.etree.ElementTree as ET

KEY_HASH = "24e0dc62a15c11d38b622162ea2b4383"
IS_UPDATED = 0
ENCRYPTED_IDS = {}

# Argument parser
parser = argparse.ArgumentParser(
    description='Automatically organize and timestamp\
                 your Nintendo Switch captures.')


# If there are arguments, parse them. If not, exit
args = parser.parse_args()


def load_key(file_name, KEY_HASH):
    try:
        with open(file_name, 'r') as key_file:
            key_string = key_file.read(32)
            key = bytes.fromhex(key_string)
            print(key)
            if(hashlib.md5(key).hexdigest() not in KEY_HASH):
                raise ValueError("Keys don't match!")
            print(key)
            return key

    except FileNotFoundError:
        print("Encryption key (key.txt) not found!")
    except ValueError:
        print("Encryption key (key.txt) doesn't match!")


def encrypt_title_id(key, title_id):
    key_cipher = AES.new(key, AES.MODE_ECB)

    title_id_bytes = bytearray.fromhex(title_id[:16])
    title_id_bytes.reverse()
    title_id_bytes = title_id_bytes.ljust(16, b'\0')  # Padding

    encrypted_title_id = key_cipher.encrypt(title_id_bytes)
    screenshot_id = encrypted_title_id.hex().upper()

    return screenshot_id


def update_game_ids():
    key = load_key('key.txt', KEY_HASH)
    if not key:
        return -1


    update_nswdb(key)


    with open('../gameids.json', 'w', encoding='utf-8') as encrypted_ids_file:
        json.dump(ENCRYPTED_IDS, encrypted_ids_file,
                  ensure_ascii=False, indent=4, sort_keys=True)
        print("Successfully updated Game IDs")

    return 1


def update_nswdb(key):
    urlretrieve('http://nswdb.com/xml.php', 'db.xml')
    tree = ET.parse('db.xml')
    root = tree.getroot()

    for release in root.findall('release'):
        try:
            screenshot_id = encrypt_title_id(key, release.find('titleid').text)

            region = release.find('region').text
            if region == "WLD":
                region = "EUR USA"

            name = release.find('name').text
            name = name.replace(":", " -")
        except ValueError:
            continue

        ENCRYPTED_IDS[screenshot_id] = re.sub("\\[Rev[^][]*]",'',name)

    os.remove("db.xml")


def check_id(game_id, ENCRYPTED_IDS):
    if game_id in ENCRYPTED_IDS:
        return ENCRYPTED_IDS[game_id]
    else:
        return 'Unknown'

# update_game_ids()
print(encrypt_title_id(load_key('key.txt', KEY_HASH),"0100A3D008C5C069"))

