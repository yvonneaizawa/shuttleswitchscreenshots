use aes::Aes128Enc;
use aes::cipher::BlockEncrypt;
use aes::cipher::generic_array::GenericArray;
use aes::cipher::KeyInit;
#[derive(Debug)]
struct CustomError {
    msg: String,
}

impl std::fmt::Display for CustomError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.msg)
    }
}
impl std::error::Error for CustomError {}

pub fn encrypt_title_id(key: &[u8], title_id: &str) -> Result<String, Box<dyn std::error::Error>> {
    //remove - from string
    let key_cipher = Aes128Enc::new(key.into());
    let mut title_id_bytes = hex::decode(&title_id[0..16])?;
    title_id_bytes.reverse();
    // Ensure the input is 16 bytes long, pad with zeros if necessary.
    title_id_bytes.resize(16, 0);
    // Encrypt the data without padding, as the input is now the correct length.
    let mut block = GenericArray::clone_from_slice(&title_id_bytes);
    key_cipher.encrypt_block(&mut block);
    Ok(hex::encode_upper(block))
}

use md5;

pub fn load_key(key: &str) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
    let key = hex::decode(key.trim()).map_err(|_| "Invalid hex format in key file")?;

    let result = md5::compute(&key);

    if format!("{:x}", result) != "24e0dc62a15c11d38b622162ea2b4383" {
        return Err(Box::new(CustomError { msg: "Encryption key (key.txt) doesn't match!".to_string() }));
    }

    Ok(key)
}

