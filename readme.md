# sys-screenuploader-servers

rust version of https://github.com/bakatrouble/sys-screenuploader-servers

made to run on https://shuttle.rs

Make sure to set secrets in the Secrets.toml.

deploy with ```cargo shuttle deploy```

to run locally run ```cargo shuttle run``` 
