impl std::error::Error for CustomError {}
use rocket::data::ToByteUnit;
use rocket::State;
use rocket::{post, routes, Data};
use serde::Deserialize;
use shuttle_rocket::ShuttleRocket;
use shuttle_runtime::SecretStore;
extern crate reqwest;
use reqwest::{multipart, Client};
use std::collections::HashMap;

use nxshot;
use quick_xml;
use std::path::Path;
#[derive(Debug)]
struct CustomError {
    msg: String,
}

impl std::fmt::Display for CustomError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.msg)
    }
}

#[post("/?<caption>&<filename>", data = "<media>")]
async fn index(
    state: &State<ShuttleSecrets>,
    ids: &State<Option<HashMap<String, Release>>>,
    media: Data<'_>,
    caption: &str,
    filename: &str,
) -> std::io::Result<String> {
    // println!("{:?}",media);
    // println!("filename: {}, caption: {}",filename,caption);

    let file = media
        .open(128.tibibytes())
        .into_bytes()
        .await
        .unwrap()
        .into_inner();
    let game_id = parse_filename(filename);
    let mut game = "Unknown";
    match game_id {
        Some(game_id_unwrapped) => {
            let unwrapped_ids = ids.as_ref();
            match unwrapped_ids {
                Some(s) => {
                    let release = s.get_key_value(game_id_unwrapped);
                    match release {
                        Some(s) => {
                            game = &s.1.name;
                        }
                        None => {}
                    }
                }
                None => {}
            }
        }
        None => {}
    }
    match get_extension_from_filename(filename) {
        Some(extension) => {
            if extension == "mp4" {
                send_video(
                    &state.chatid,
                    &state.token,
                    filename.to_string(),
                    file,
                    game.to_string(),
                )
                .await;
                return Ok("sent".to_string());
            } else if extension == "jpg" {
                send_photo(
                    &state.chatid,
                    &state.token,
                    filename.to_string(),
                    file,
                    game.to_string(),
                )
                .await;

                return Ok("sent".to_string());
            } else {
                return Ok("File extension not supported".to_string());
            }
        }
        None => {
            return Ok("No file extension found".to_string());
        }
    }
}
fn get_extension_from_filename(filename: &str) -> Option<String> {
    Path::new(filename)
        .extension()
        .map(|ex| ex.to_str().unwrap().to_owned())
}
fn parse_filename(filename: &str) -> Option<&str> {
    let parts: Vec<&str> = filename.split('-').collect();
    if parts.len() > 1 {
        let second_part: Vec<&str> = parts[1].split('.').collect();
        if second_part.len() > 1 {
            return Some(second_part[0]);
        }
    }
    None
}
async fn send_photo(
    chat_id: &String,
    bot_token: &String,
    filename: String,
    photo: Vec<u8>,
    game: String,
) {
    let mut client = Client::new();
    let url = format!("https://api.telegram.org/bot{}/sendPhoto", bot_token);
    let image = multipart::Part::stream(photo)
        .file_name(filename)
        .mime_str("image/jpeg")
        .unwrap();
    let form = multipart::Form::new()
        .text("chat_id", format!("{}", chat_id))
        .text("caption", format!("{}", game))
        .part("photo", image);
    let res = client.post(url).multipart(form).send().await;
}
async fn send_video(
    chat_id: &String,
    bot_token: &String,
    filename: String,
    video: Vec<u8>,
    game: String,
) {
    let client = Client::new();
    let url = format!("https://api.telegram.org/bot{}/sendVideo", bot_token);
    let media = multipart::Part::stream(video)
        .file_name("video")
        .mime_str("video/mp4")
        .unwrap();
    let form = multipart::Form::new()
        .text("chat_id", format!("{}", chat_id))
        .text("caption", format!("{}", game))
        .part("video", media);
    let res = client.post(url).multipart(form).send().await;
}

struct ShuttleSecrets {
    chatid: String,
    token: String,
    capserv_key: String,
}

// Correct the Result type to use only one generic argument
async fn download_title_ids(key: &[u8]) -> Option<HashMap<String, Release>> {
    let resp = reqwest::get("http://nswdb.com/xml.php").await;
    match resp {
        Ok(o) => {
            let releases: Releases = quick_xml::de::from_str(&o.text().await.unwrap()).unwrap();

            let mut map = HashMap::new();
            for release in releases.release {
                let encrypted_id = nxshot::encrypt_title_id(&key, &release.title_id);
                if encrypted_id.is_ok() {
                    map.insert(encrypted_id.unwrap(), release);
                }
            }

            Some(map)
        }
        Err(e) => None,
    }
}
#[derive(Debug, Deserialize)]
struct Release {
    #[serde(rename = "titleid")]
    title_id: String,
    name: String,
}
#[derive(Debug, Deserialize)]
struct Releases {
    release: Vec<Release>,
}
#[shuttle_runtime::main]
async fn rocket(
    #[shuttle_runtime::Secrets] secret_store: SecretStore,
) -> ShuttleRocket {
    let secrets = ShuttleSecrets {
        chatid: secret_store.get("CHAT_ID").unwrap(),
        token: secret_store.get("TOKEN").unwrap(),
        capserv_key: secret_store.get("CAPSERV_KEY").unwrap_or("".to_string()),
    };
    // Before the call to download_title_ids, store the key in a variable
    let key = nxshot::load_key(&secrets.capserv_key).unwrap();
    let ids = download_title_ids(&key).await;
    let rocket = rocket::build()
        .mount("/", routes![index])
        .manage(secrets)
        .manage(ids);

    Ok(rocket.into())
}